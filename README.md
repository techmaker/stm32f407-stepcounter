# STM32F407 stepcounter project
Based on STM32F407VE black board with FSMC display. 
Uses MPU6050 with eMD6 library + INT pin to capture acc data and stepcounter info. 
Logs data onto SD Card with FatFs via 4-bit SDIO (in DMA mode).
Project runs under FreeRTOS.