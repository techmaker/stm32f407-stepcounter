/* USER CODE BEGIN Header */
/**
 ******************************************************************************
 * File Name          : freertos.c
 * Description        : Code for freertos applications
 ******************************************************************************
 * @attention
 *
 * <h2><center>&copy; Copyright (c) 2019 STMicroelectronics.
 * All rights reserved.</center></h2>
 *
 * This software component is licensed by ST under Ultimate Liberty license
 * SLA0044, the "License"; You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *                             www.st.com/SLA0044
 *
 ******************************************************************************
 */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "FreeRTOS.h"
#include "task.h"
#include "main.h"
#include "cmsis_os.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "printf/printf.h"
#include "dma.h"
#include "fatfs.h"
#include "i2c.h"
#include "sdio.h"
#include "gpio.h"
#include "fsmc.h"
#include "lcd.h"
#include "eMD6.h"
#include "graph.h"
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */
enum {
	sig_Terminate = 0x01,
	sig_MPU = 0x02,
	sig_LCD = 0x04,
};
/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */

/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
/* USER CODE BEGIN Variables */
osMailQId mail_LCDHandle;
osMailQId mail_SDHandle;
/* USER CODE END Variables */
osThreadId thr_MPUHandle;
osThreadId thr_SDHandle;
osThreadId thr_LCDHandle;

/* Private function prototypes -----------------------------------------------*/
/* USER CODE BEGIN FunctionPrototypes */
/* Debug output via printf() and SWO if needed */
#ifdef DEBUG_PRINT_SWO
void _putchar(char character) {
	ITM_SendChar(character);
}
#endif
/* USER CODE END FunctionPrototypes */

void MPU_Thread(void const* argument);
void SD_Thread(void const* argument);
void LCD_Thread(void const* argument);

void MX_FREERTOS_Init(void); /* (MISRA C 2004 rule 8.1) */

/* Hook prototypes */
void vApplicationStackOverflowHook(xTaskHandle xTask, signed char *pcTaskName);
void vApplicationMallocFailedHook(void);

/* USER CODE BEGIN 4 */
__weak void vApplicationStackOverflowHook(xTaskHandle xTask, signed char *pcTaskName) {
	/* Run time stack overflow checking is performed if
	 configCHECK_FOR_STACK_OVERFLOW is defined to 1 or 2. This hook function is
	 called if a stack overflow is detected. */
	Error_Handler();
}
/* USER CODE END 4 */

/* USER CODE BEGIN 5 */
__weak void vApplicationMallocFailedHook(void) {
	/* vApplicationMallocFailedHook() will only be called if
	 configUSE_MALLOC_FAILED_HOOK is set to 1 in FreeRTOSConfig.h. It is a hook
	 function that will get called if a call to pvPortMalloc() fails.
	 pvPortMalloc() is called internally by the kernel whenever a task, queue,
	 timer or semaphore is created. It is also called by various parts of the
	 demo application. If heap_1.c or heap_2.c are used, then the size of the
	 heap available to pvPortMalloc() is defined by configTOTAL_HEAP_SIZE in
	 FreeRTOSConfig.h, and the xPortGetFreeHeapSize() API function can be used
	 to query the size of free heap space that remains (although it does not
	 provide information on how the remaining heap might be fragmented). */
	Error_Handler();
}
/* USER CODE END 5 */

/**
 * @brief  FreeRTOS initialization
 * @param  None
 * @retval None
 */
void MX_FREERTOS_Init(void) {
	/* USER CODE BEGIN Init */
#ifdef USE_LCD
	LCD_Init();
	LCD_SetTextSize(1);
	LCD_SetRotation(1);
#endif
	/* Enable DWT module */
	CoreDebug->DEMCR |= CoreDebug_DEMCR_TRCENA_Msk;
	/* Enable CYCCNT cycle counter */
	DWT->CTRL |= DWT_CTRL_CYCCNTENA_Msk;
	/* USER CODE END Init */

	/* USER CODE BEGIN RTOS_MUTEX */
	/* add mutexes, ... */
	/* USER CODE END RTOS_MUTEX */

	/* USER CODE BEGIN RTOS_SEMAPHORES */
	/* add semaphores, ... */
	/* USER CODE END RTOS_SEMAPHORES */

	/* USER CODE BEGIN RTOS_TIMERS */
	/* start timers, add new ones, ... */
	/* USER CODE END RTOS_TIMERS */

	/* USER CODE BEGIN RTOS_QUEUES */
	osMailQDef(mail_SD, 16, eMD6_data_t);
	mail_SDHandle = osMailCreate(osMailQ(mail_SD), NULL);

	osMailQDef(mail_LCD, 16, eMD6_data_t);
#ifdef USE_LCD
	mail_LCDHandle = osMailCreate(osMailQ(mail_LCD), NULL);
#endif
	/* USER CODE END RTOS_QUEUES */

	/* Create the thread(s) */
	/* definition and creation of thr_MPU */
	osThreadDef(thr_MPU, MPU_Thread, osPriorityNormal, 0, 1024);
	thr_MPUHandle = osThreadCreate(osThread(thr_MPU), NULL);

	/* definition and creation of thr_SD */
	osThreadDef(thr_SD, SD_Thread, osPriorityHigh, 0, 2048);
	thr_SDHandle = osThreadCreate(osThread(thr_SD), NULL);

	/* definition and creation of thr_LCD */
	osThreadDef(thr_LCD, LCD_Thread, osPriorityLow, 0, 4096);
#ifdef USE_LCD
	thr_LCDHandle = osThreadCreate(osThread(thr_LCD), NULL);
#endif
	/* USER CODE BEGIN RTOS_THREADS */
	/* add threads, ... */
	/* USER CODE END RTOS_THREADS */

}

/* USER CODE BEGIN Header_MPU_Thread */
/**
 * @brief  Function implementing the thr_MPU thread.
 * @param  argument: Not used
 * @retval None
 */
/* USER CODE END Header_MPU_Thread */
void MPU_Thread(void const *argument) {
	/* USER CODE BEGIN MPU_Thread */
	uint32_t sample = 0;
	eMD6_data_t eMD6_data = { 0 };
	uint8_t eMD6_data_ready = 0;
	if (eMD6_Init(&hi2c1)) {
#ifdef USE_LCD
		LCD_Printf("eMD6 init failed!");
#endif
		Error_Handler();
	}
	osEvent evt;
	/* Infinite loop */
	for (;;) {
		/* Get new data from MPU sensor */
		evt = osSignalWait(0, 500);
		if (evt.status == osEventSignal) {
			if (evt.value.signals & sig_MPU) {
				eMD6_DataReadyCallback();
				inv_error_t eMD6_result = eMD6_Process(&eMD6_data_ready);
				if (eMD6_result == INV_SUCCESS && eMD6_data_ready) {
					if (eMD6_GetData(&eMD6_data) == INV_SUCCESS) {
						sample++;
						/* Allocate new cells in mail queues */
						eMD6_data_t* sd_data = (eMD6_data_t*) osMailAlloc(mail_SDHandle, 10);
						if (sd_data != NULL) {
							memcpy(sd_data, &eMD6_data, sizeof(eMD6_data));
							/* Put the mail in the queue */
							osMailPut(mail_SDHandle, sd_data);
						}
						if (sample % 4 == 0) {
							eMD6_data_t* lcd_data = (eMD6_data_t*) osMailAlloc(mail_LCDHandle, 5);
							if (lcd_data != NULL) {
								memcpy(lcd_data, &eMD6_data, sizeof(eMD6_data));
								/* Put the mail in the queue */
								osMailPut(mail_LCDHandle, lcd_data);
							}
						}
					}
				}
			}
			if (evt.value.signals & sig_Terminate) {
				/* Check if need to terminate */
				eMD6_DeInit();
				osThreadTerminate(NULL);
			}
		}
	}
	/* USER CODE END MPU_Thread */
}

/* USER CODE BEGIN Header_SD_Thread */
/**
 * @brief Function implementing the thr_SD thread.
 * @param argument: Not used
 * @retval None
 */
/* USER CODE END Header_SD_Thread */
void SD_Thread(void const *argument) {
	/* USER CODE BEGIN SD_Thread */
	uint8_t eMD6_str[_MAX_SS] = { 0 };
	size_t eMD6_strlen = 0;
	MX_FATFS_Init();
	osEvent evt;
	/* Infinite loop */
	for (;;) {
		/* Get new mail from the queue */
		evt = osMailGet(mail_SDHandle, 500);
		if (evt.status == osEventMail) {
			/* Obtain data from mail */
			eMD6_data_t* eMD6_data = (eMD6_data_t*) evt.value.p;
			if (eMD6_data != NULL) {
				eMD6_strlen = snprintf((char*) eMD6_str, sizeof(eMD6_str), // @suppress("Float formatting support")
										"%f,"
										"%f,%f,%f,"
										"%f,%f,%f,"
										"%f,%f,%f,"
										"%lu,%f\r\n",
										eMD6_data->timestamp / 1000.f,
										eMD6_data->acc_raw[0], eMD6_data->acc_raw[1], eMD6_data->acc_raw[2],
										eMD6_data->gyro[0], eMD6_data->gyro[1], eMD6_data->gyro[2],
										eMD6_data->acc_lin[0], eMD6_data->acc_lin[1], eMD6_data->acc_lin[2],
										eMD6_data->step_count, eMD6_data->step_time / 1000.f
										);
				SD_WriteLog(eMD6_str, eMD6_strlen);
			}
			/* Free the cell in mail queue */
			osMailFree(mail_SDHandle, eMD6_data);
		}
		/* Check if need to terminate */
		evt = osSignalWait(0, 0);
		if (evt.status == osEventSignal) {
			if (evt.value.signals & sig_Terminate) {
				SD_CloseLog();
				osThreadTerminate(NULL);
			}
		}
	}
	/* USER CODE END SD_Thread */
}

/* USER CODE BEGIN Header_LCD_Thread */
/**
 * @brief Function implementing the thr_LCD thread.
 * @param argument: Not used
 * @retval None
 */
/* USER CODE END Header_LCD_Thread */
void LCD_Thread(void const *argument) {
	/* USER CODE BEGIN LCD_Thread */
	uint8_t lcd_axes[] = {	LCD_Graph_XMask | LCD_Graph_YMask | LCD_Graph_ZMask,
							LCD_Graph_XMask,
							LCD_Graph_YMask,
							LCD_Graph_ZMask,
							LCD_Graph_XMask | LCD_Graph_YMask,
							LCD_Graph_XMask | LCD_Graph_ZMask,
							LCD_Graph_YMask | LCD_Graph_ZMask,
							0 };
	uint8_t lcd_axis_num = 0;
	osEvent evt;
	/* Infinite loop */
	for (;;) {
		/* Get new mail from the queue */
		evt = osMailGet(mail_LCDHandle, 500);
		if (evt.status == osEventMail) {
			/* Obtain data from mail */
			eMD6_data_t* eMD6_data = (eMD6_data_t*) evt.value.p;
			if (eMD6_data != NULL) {
				LCD_Graph_Draw(eMD6_data, lcd_axes[lcd_axis_num]);
//				LCD_SetCursor(0, 0);
//				LCD_Printf((char*) eMD6_str);
			}
			/* Free the cell in mail queue */
			osMailFree(mail_LCDHandle, eMD6_data);
		}
		evt = osSignalWait(0, 0);
		if (evt.status == osEventSignal) {
			/* Check if axes changed */
			if (evt.value.signals & sig_LCD) {
				lcd_axis_num = (lcd_axis_num + 1) % (sizeof(lcd_axes) / sizeof(lcd_axes[0]));
				LCD_FillScreen(BLACK);

			}
			if (evt.value.signals & sig_Terminate) {
				/* Check if need to terminate */
				LED2_ON();
				LED3_ON();
				LCD_SetTextSize(2);
				LCD_SetCursor(0, 0);
				LCD_Printf("\n\n\n\n\n");
				LCD_Printf(" =========================== ");
				LCD_Printf(" |   PROGRAM TERMINATED.   | ");
				LCD_Printf(" | SD CARD CAN BE EJECTED! | ");
				LCD_Printf(" =========================== ");
				LCD_SetTextSize(1);
				osThreadTerminate(NULL);
			}
		}
	}
	/* USER CODE END LCD_Thread */
}

/* Private application code --------------------------------------------------*/
/* USER CODE BEGIN Application */
void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin) {
	static uint32_t btn_timestamp, press_timestamp;
	uint32_t cur_timestamp = HAL_GetTick();
	if (osKernelRunning()) {
		switch (GPIO_Pin) {
		case MPU_INT_Pin:
			osSignalSet(thr_MPUHandle, sig_MPU);
			break;
		case KEY1_Pin:
			if (cur_timestamp - btn_timestamp > 25) {
				if (HAL_GPIO_ReadPin(KEY1_GPIO_Port, KEY1_Pin)) {
					/* release */
					if (cur_timestamp - press_timestamp > 1500) {
						/* long */
						osSignalSet(thr_MPUHandle, sig_Terminate);
						osSignalSet(thr_SDHandle, sig_Terminate);
						osSignalSet(thr_LCDHandle, sig_Terminate);
					} else {
						/* short */
						osSignalSet(thr_LCDHandle, sig_LCD);
					}
				} else {
					/* press */
					press_timestamp = cur_timestamp;
				}
			}
			btn_timestamp = cur_timestamp;
			break;
		default:
			break;
		}
	}
}
/* USER CODE END Application */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
