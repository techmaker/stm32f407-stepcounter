/*
 * graph.c
 *
 *  Created on: Dec 6, 2019
 *      Author: alexa
 */

#include "graph.h"


LCD_graph_data_t eMD6_graph[LCD_Graph_Max] = {
	[LCD_Graph_X] = {
		.raw_data = {0},
		.plot_data = {0},
		.last = 0,
		.total = LCD_GRAPH_XNUM,
		.x_inc = LCD_GRAPH_INC,
		.y_scale = LCD_GRAPH_YSIZE / 4.f,
		.y_offset = LCD_GRAPH_YSIZE / 2
	},
	[LCD_Graph_Y] = {
		.raw_data = {0},
		.plot_data = {0},
		.last = 0,
		.total = LCD_GRAPH_XNUM,
		.x_inc = LCD_GRAPH_INC,
		.y_scale = LCD_GRAPH_YSIZE / 4.f,
		.y_offset = LCD_GRAPH_YSIZE / 2
	},
	[LCD_Graph_Z] = {
		.raw_data = {0},
		.plot_data = {0},
		.last = 0,
		.total = LCD_GRAPH_XNUM,
		.x_inc = LCD_GRAPH_INC,
		.y_scale = LCD_GRAPH_YSIZE / 4.f,
		.y_offset = LCD_GRAPH_YSIZE / 2
	},
};

static inline size_t countSetBits(uint16_t n);
static inline void plotAxisHelper(LCD_graph_data_t* graph, int16_t x, int16_t y, int16_t y_factor, int16_t color, int16_t first_offset);

void LCD_Graph_AddDatum(LCD_graph_data_t* graph, float datum) {
	graph->last++;
	if (graph->last >= graph->total) {
		graph->last = 0;
	}
	graph->raw_data[graph->last] = datum;
	graph->plot_data[graph->last] = graph->y_offset - datum * graph->y_scale;
}

void LCD_Graph_PlotAxis(LCD_graph_data_t* graph, int16_t x, int16_t y, int16_t y_factor, int16_t color, int16_t bg_color, char* label) {
	const int16_t gridcolor = DARKGREY;

	if (y_factor == 0) {
		return;
	}
//	LCD_FillRect(x, y, LCD_GRAPH_XSIZE, LCD_GRAPH_YSIZE / y_factor, bg_color);
	LCD_DrawFastHLine(x, y + graph->y_offset / y_factor, LCD_GRAPH_XSIZE + 6, gridcolor);
	LCD_DrawFastHLine(LCD_GRAPH_XSIZE, y + graph->y_offset / y_factor + 2 * graph->y_scale / y_factor, 4, gridcolor);
	LCD_DrawFastHLine(LCD_GRAPH_XSIZE, y + graph->y_offset / y_factor + 1 * graph->y_scale / y_factor, 4, gridcolor);
	LCD_DrawFastHLine(LCD_GRAPH_XSIZE, y + graph->y_offset / y_factor - 1 * graph->y_scale / y_factor, 4, gridcolor);
	LCD_DrawFastHLine(LCD_GRAPH_XSIZE, y + graph->y_offset / y_factor - 2 * graph->y_scale / y_factor, 4, gridcolor);
	LCD_DrawFastVLine(LCD_GRAPH_XSIZE, y, LCD_GRAPH_YSIZE / y_factor, gridcolor);

	plotAxisHelper(graph, x, y, y_factor, bg_color, 1);
	plotAxisHelper(graph, x, y, y_factor, color, 2);

	LCD_SetTextSize(2);
	LCD_SetCursor(LCD_GRAPH_XSIZE + 9, y + graph->y_offset / y_factor - 7);
	LCD_Printf(label);
	LCD_SetTextSize(1);
	LCD_SetCursor(LCD_GRAPH_XSIZE - 40, y + 5);
	LCD_Printf("%5.2f", graph->raw_data[graph->last]);
}

void LCD_Graph_Draw(eMD6_data_t* data, uint8_t axes) {
	LCD_Graph_AddDatum(&eMD6_graph[0], data->acc_raw[0]);
	LCD_Graph_AddDatum(&eMD6_graph[1], data->acc_raw[1]);
	LCD_Graph_AddDatum(&eMD6_graph[2], data->acc_raw[2]);
	if (axes == 0) {
		return;
	}
	uint32_t factor = countSetBits(axes);
	uint32_t axisnum = 0;
	if (axes & LCD_Graph_XMask) {
		LCD_Graph_PlotAxis(&eMD6_graph[LCD_Graph_X], 0, axisnum++ * LCD_GRAPH_YSIZE / factor, factor, RED, BLACK, "X");
	}
	if (axes & LCD_Graph_YMask) {
		LCD_Graph_PlotAxis(&eMD6_graph[LCD_Graph_Y], 0, axisnum++ * LCD_GRAPH_YSIZE / factor, factor, GREEN, BLACK, "Y");
	}
	if (axes & LCD_Graph_ZMask) {
		LCD_Graph_PlotAxis(&eMD6_graph[LCD_Graph_Z], 0, axisnum++ * LCD_GRAPH_YSIZE / factor, factor, BLUE, BLACK, "Z");
	}
}

/* Function to get no of set bits in binary
   representation of passed binary no. */
static inline size_t countSetBits(uint16_t n) {
    size_t count = 0;
    while (n) {
        n &= (n - 1);
        count++;
    }
    return count;
}

static inline void plotAxisHelper(LCD_graph_data_t* graph, int16_t x, int16_t y, int16_t y_factor, int16_t color, int16_t first_offset) {
	if (first_offset == 0) {
		return;
	}
	int16_t graph_x = 0;
	int32_t first = (graph->last + first_offset) % graph->total;
	int32_t last = graph->last + first_offset - 2;
	if (last < 0) {
		last += graph->total;
	} else if (last >= graph->total) {
		last -= graph->total;
	}
	for (size_t i = first + 1; i < graph->total - 1; i++) {
		LCD_DrawLine(graph_x,
				(int16_t) (y + graph->plot_data[i - 1] / y_factor),
				graph_x + graph->x_inc,
				(int16_t) (y + graph->plot_data[i] / y_factor),
				color);
		graph_x += graph->x_inc;
	}
	if (last < first) {		LCD_DrawLine(graph_x,
			(int16_t) (y + graph->plot_data[graph->total - 2] / y_factor),
			graph_x + graph->x_inc,
			(int16_t) (y + graph->plot_data[graph->total - 1] / y_factor),
			color);
	graph_x += graph->x_inc;
		LCD_DrawLine(graph_x,
				(int16_t) (y + graph->plot_data[graph->total - 1] / y_factor),
				graph_x + graph->x_inc,
				(int16_t) (y + graph->plot_data[0] / y_factor),
				color);
		graph_x += graph->x_inc;
		for (size_t i = 1; i <= last; i++) {
			LCD_DrawLine(graph_x,
					(int16_t) (y + graph->plot_data[i - 1] / y_factor),
					graph_x + graph->x_inc,
					(int16_t) (y + graph->plot_data[i] / y_factor),
					color);
			graph_x += graph->x_inc;
		}
	}
}
