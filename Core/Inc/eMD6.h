/*
 * eMD6.h
 *
 *  Created on: Dec 5, 2019
 *      Author: alexa
 */

#ifndef INC_EMD6_H_
#define INC_EMD6_H_

#include "main.h"
#include "mltypes.h"

typedef struct {
	uint32_t timestamp;
	float acc_raw[3];
	float acc_lin[3];
	float gyro[3];
	uint32_t step_count;
	uint32_t step_time;
} eMD6_data_t;

inv_error_t eMD6_Init(I2C_HandleTypeDef *hi2c);
inv_error_t eMD6_DeInit(void);
inv_error_t eMD6_Process(uint8_t* newdata);
inv_error_t eMD6_GetData(eMD6_data_t* data);
void eMD6_DataReadyCallback(void);

#endif /* INC_EMD6_H_ */
