/*
 * graph.h
 *
 *  Created on: Dec 6, 2019
 *      Author: alexa
 */

#ifndef INC_GRAPH_H_
#define INC_GRAPH_H_

#include "eMD6.h"
#include "lcd.h"

#define LCD_GRAPH_XSIZE	(TFTHEIGHT - 25)
#define LCD_GRAPH_YSIZE (TFTWIDTH)
#define LCD_GRAPH_INC	2
#define LCD_GRAPH_XNUM	(LCD_GRAPH_XSIZE / LCD_GRAPH_INC)

typedef struct {
	float raw_data[LCD_GRAPH_XNUM];
	int16_t plot_data[LCD_GRAPH_XNUM];
	size_t last;
	const size_t total;
	const int16_t x_inc;
	const float y_scale;
	const int16_t y_offset;
} LCD_graph_data_t;

typedef enum {
	LCD_Graph_X,
	LCD_Graph_Y,
	LCD_Graph_Z,
	LCD_Graph_Max,
	LCD_Graph_XMask = 1 << (LCD_Graph_X + LCD_Graph_Max),
	LCD_Graph_YMask = 1 << (LCD_Graph_Y + LCD_Graph_Max),
	LCD_Graph_ZMask = 1 << (LCD_Graph_Z + LCD_Graph_Max),
} LCD_graph_axes_t;

void LCD_Graph_AddDatum(LCD_graph_data_t* graph, float datum);
void LCD_Graph_PlotAxis(LCD_graph_data_t* graph, int16_t x, int16_t y, int16_t y_factor, int16_t color, int16_t bg_color, char* label);
void LCD_Graph_Draw(eMD6_data_t* data, uint8_t axes);

#endif /* INC_GRAPH_H_ */
