/*
 * MPU_drv.h
 *
 */

#ifndef __MPU_DRV_H
#define __MPU_DRV_H

#include "stm32f4xx.h"

void mpu_i2c_init(I2C_HandleTypeDef *hi2c);
int mpu_i2c_write(unsigned char slave_addr, unsigned char reg_addr, unsigned char length, unsigned char const *data);
int mpu_i2c_read(unsigned char slave_addr, unsigned char reg_addr, unsigned char length, unsigned char *data);

#endif /* __MPU_DRV_H */
