/**
  ******************************************************************************
  * @file   fatfs.c
  * @brief  Code for fatfs applications
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2019 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under Ultimate Liberty license
  * SLA0044, the "License"; You may not use this file except in compliance with
  * the License. You may obtain a copy of the License at:
  *                             www.st.com/SLA0044
  *
  ******************************************************************************
  */

#include "fatfs.h"

uint8_t retSD;    /* Return value for SD */
char SDPath[4];   /* SD logical drive path */
FATFS SDFatFS;    /* File system object for SD logical drive */
FIL SDFile;       /* File object for SD */

/* USER CODE BEGIN Variables */
static bool fmounted = false;
static bool fopened = false;
static uint8_t fname[256] = { 0 };
static int32_t fnumber = 0;
/* Mount SD card */
static FRESULT SD_Mount(FATFS* fs);
/* Reinitialize SD card in case of critical error i.e. FR_NOT_READY, FR_DISK_ERR */
static FRESULT SD_ReInit(FATFS* fs);
/* Open existing file and append / create new */
static FRESULT SD_OpenFile(FIL* fr, uint8_t * filename);
/* Scan through root to find filenames matching pattern log_****.csv */
static uint32_t SD_GetLogNum(void);
/* USER CODE END Variables */    

void MX_FATFS_Init(void) 
{
  /*## FatFS: Link the SD driver ###########################*/
  retSD = FATFS_LinkDriver(&SD_Driver, SDPath);

  /* USER CODE BEGIN Init */
  /* additional user code for init */     
  /* USER CODE END Init */
}

/* USER CODE BEGIN Application */
void MX_FATFS_DeInit(void)
{
  /*## FatFS: DeLink the USER driver ###########################*/
  retSD = FATFS_UnLinkDriver(SDPath);
}

/* Write text onto sd card*/
FRESULT SD_WriteLog(uint8_t* data, size_t len) {
	size_t fwritten = 0;
	FRESULT fres = FR_OK;
	FRESULT ret = FR_INT_ERR;

	if (!fmounted) {				// try to mount SD card
		fres = SD_Mount(&SDFatFS);
		if (fres != FR_OK) {		// try to reinit SD card
			fres = SD_ReInit(&SDFatFS);
		} else {
			fnumber = SD_GetLogNum() + 1;		// find latest filename
		}
		if (fres == FR_OK && fnumber >= 0) {
			fmounted = true;
		} else {
			fmounted = false;
		}
	}
	if (fmounted) {
		if (!fopened) { // try to open file
			snprintf((char *) fname, sizeof(fname), "log_%04lu.csv", fnumber);
			if (SD_OpenFile(&SDFile, fname) == FR_OK) {
				fopened = true;
			} else {				// try to remount SD card
				f_close(&SDFile);
				fopened = false;
				fmounted = false;
			}
		}
		if (fopened) {
			fres = FR_OK;
			LED2_ON();
			fres += f_write(&SDFile, data, len, (UINT *) &fwritten);
//			fres += f_sync(&SDFile);
			LED2_OFF();
			if (fres != FR_OK) {	// try to remount SD card
				f_close(&SDFile);
				fopened = false;
				fmounted = false;
			} else {
				ret = FR_OK;
			}
			if (f_size(&SDFile) >= 0x8000000) {	// if file bigger 128MB then create new
				fnumber++;			// increment filenumber
				fnumber %= 10000;	// end numbering in 9999
				f_close(&SDFile);
				fopened = false;
			}
		}
	}
	return ret;
}

FRESULT SD_CloseLog(void) {
	FRESULT fres = f_close(&SDFile);
	if (fres == FR_OK) {
		fopened = false;
	}
	return fres;
}

/* Mount SD card */
static FRESULT SD_Mount(FATFS* fs) {
	FRESULT fres = FR_OK;
	f_mount(NULL, (const TCHAR *) L"", 0);
	fres = f_mount(fs, (const TCHAR *) L"", 1);
	return fres;
}


/* Reinitialize SD card in case of critical error i.e. FR_NOT_READY, FR_DISK_ERR */
static FRESULT SD_ReInit(FATFS* fs) {
	FRESULT fres = FR_OK;
	LED3_ON();
	f_mount(NULL, (const TCHAR *) L"", 0);
	MX_FATFS_DeInit();
	MX_FATFS_Init();
	fres = SD_Mount(fs);
	if (fres == FR_OK) {
		LED3_OFF();
	}
	return fres;
}

/* Open existing file and append / create new */
static FRESULT SD_OpenFile(FIL* fr, uint8_t * filename) {
	FRESULT fres = FR_OK;
	LED3_ON();
//	fres += f_open(fr, (const TCHAR *) filename, FA_OPEN_ALWAYS | FA_WRITE);
//	fres += f_lseek(fr, f_size(fr));
	fres += f_open(fr, (const TCHAR *) filename, FA_OPEN_APPEND | FA_WRITE);
	if (fres == FR_OK) {
		LED3_OFF();
	}
	return fres;
}

/* Scan through root to find filenames matching pattern log_****.csv */
static uint32_t SD_GetLogNum(void) {
	DIR dir;
	FILINFO finfo;
	FRESULT fres = FR_OK;
	int32_t fnumber = 0;
	int8_t template[255] = "log_****.csv";
	fres += f_opendir(&dir, (const TCHAR *) L"");
	if (fres != FR_OK) {
		return -1;
	}
	do {
		fres += f_readdir(&dir, &finfo);
		uint32_t errors = 0;
		for (uint32_t i = 0; i < 13; i++) {
			if (i < 4 || i > 8) { // test for LOG_ in beginning and TXT in the end
				errors += (finfo.fname[i] != template[i]);
			}
		}
		if (errors != 0) {
			continue;
		}
		uint32_t fnumber_cur = 0;
		for (uint8_t i = 4; i < 8; i++) {
			fnumber_cur = fnumber_cur * 10 + (finfo.fname[i] - '0');
		}
		if (fnumber <= fnumber_cur) {
			fnumber = fnumber_cur;
		}
	} while (fres == FR_OK && finfo.fname[0] != 0);
	f_closedir(&dir);
	if (fres == FR_OK) {
		return fnumber;
	} else {
		return -1;
	}
}
/* USER CODE END Application */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
